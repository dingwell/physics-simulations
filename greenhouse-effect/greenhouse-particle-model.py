#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from matplotlib.patches import Polygon
from matplotlib import animation
from itertools import combinations
from itertools import chain

from time import sleep
DEBUG = False    # Adds a time delay between frames

class Particle:
    """A class representing a two-dimensional particle."""

    def __init__(self, x, y, vx, vy, radius=0.01, vibrating=1, active=False, styles=None):
        """Initialize the particle's position, velocity, and radius.

        Any key-value pairs passed in the styles dictionary will be passed
        as arguments to Matplotlib's Circle patch constructor.

        """

        self.r = np.array((x, y))
        self.v = np.array((vx, vy))
        self.radius = radius
        
        self.vibrating = vibrating  # Has vibrational energy? (int)
        self.active = active # Interacts with light? (bool)

        self.styles = styles
        if not self.styles:
            # Default circle styles
            self.styles = {'edgecolor': 'b', 'fill': False}

    # For convenience, map the components of the particle's position and
    # velocity vector onto the attributes x, y, vx and vy.
    @property
    def x(self):
        return self.r[0]
    @x.setter
    def x(self, value):
        self.r[0] = value
    @property
    def y(self):
        return self.r[1]
    @y.setter
    def y(self, value):
        self.r[1] = value
    @property
    def vx(self):
        return self.v[0]
    @vx.setter
    def vx(self, value):
        self.v[0] = value
    @property
    def vy(self):
        return self.v[1]
    @vy.setter
    def vy(self, value):
        self.v[1] = value

    def overlaps(self, other):
        """Does the circle of this Particle overlap that of other?"""

        return np.hypot(*(self.r - other.r)) < self.radius + other.radius

    def draw(self, ax):
        """Add this Particle's Circle patch to the Matplotlib Axes ax."""

        circle = Circle(xy=self.r, radius=self.radius, **self.styles)
        ax.add_patch(circle)
        return circle

    def advance(self, dt):
        """Advance the Particle's position forward in time by dt."""

        self.r += self.v * dt

        # Make the Particles bounce off the walls
        if self.x - self.radius < 0:
            self.x = self.radius
            self.vx = -self.vx
        if self.x + self.radius > 1:
            self.x = 1-self.radius
            self.vx = -self.vx
        if self.y - self.radius < 0:
            self.y = self.radius
            self.vy = -self.vy
        if self.y + self.radius > 1:
            self.y = 1-self.radius
            self.vy = -self.vy
            
    def excited(self):
        """Checks particle velocity to determine if it can radiate
        
        if greater than set value, return true.
        """
        #v_abs = np.linalg.norm(self.v)
        #if v_abs > 0.2:
        #    return True
        if self.vibrating >= 1:
            return True

class Ray:
    """A class representing a ray of light."""
    def __init__(self, x, y, vx, vy, active = True, styles=None):
        """Initialize the ray position, velocity, and radius.

        Any key-value pairs passed in the styles dictionary will be passed
        as arguments to Matplotlib's Line patch constructor.

        """
        self.active = active
        self.r = np.array([[x, y],[x,y]])
        self.v = np.array((vx, vy))

        self.styles = styles
        if not self.styles:
            # Default arrow styles
            self.styles = {'ec': '#cccc00', 'lw': 5.0, 'ls' : '-', 'alpha' : 0.6}
            
    # For convenience, map the components of the ray's position and
    # velocity vector onto the attributes x, y, vx and vy.
    
    @property
    def x(self):
        return self.r[0,0]
    @x.setter
    def x(self, value):
        self.r[0,0] = value
    @property
    def y(self):
        return self.r[0,1]
    @y.setter
    def y(self, value):
        self.r[0,1] = value
    @property
    def vxy(self):
        return self.v
    @vxy.setter
    def vxy(self, value):
        self.v = value
    @property
    def vx(self):
        return self.v[0]
    @vx.setter
    def vx(self, value):
        self.v[0] = value
    @property
    def vy(self):
        return self.v[1]
    @vy.setter
    def vy(self, value):
        self.v[1] = value
        
    def hit(self, other):
        """Does the front end of this Ray overlap with the other object?"""
        return np.hypot(*(self.r[0,:] - other.r)) <  other.radius
        
    def draw(self, ax):
        """Add this Ray's patch to the Matplotlib Axes ax."""
        # Only draw Ray if it is set as "active"
        #if self.active:
        polygon = Polygon(self.r,closed=False, **self.styles)

        ax.add_patch(polygon)
        return polygon
        
    def advance(self, dt):
        """Advance the Particle's position forward in time by dt."""
        self.r[0,:] += self.v * dt

        # Terminate Ray if it leaves the frame
        if self.active == True:
            if self.x < 0 or self.y < 0 or self.x > 1 or self.y > 1:
                self.x0 = self.x
                self.y0 = self.y
                self.active = False
        else:
            self.r[:] = -1
            self.v[:] = -1
            

class Simulation:
    """A class for a particle atmosphere simulation with selective
    light absorption and emission.

    The simulation is carried out on a square domain: 0 <= x < 1, 0 <= y < 1.

    """

    def __init__(self,
        n_passive = 10, radius_passive=0.01, styles_passive=None,
        n_active = 1,  radius_active=0.01,  styles_active=None,
        n_rays = 5, ray_freq=0.5):
        """Initialize the simulation with n Particles with radii radius.

        radius can be a single value or a sequence with n values.
        
        ray_emit_test sets the likehood of light rays being emitted.
        A value of 1.0 will ensure that all of the allowed n_rays are
        always active.
        
        This class simulates both passive particles which will not
        interact with light rays and active particles which will absorb
        and emit light.

        Any key-value pairs passed in the styles dictionary will be passed
        as arguments to Matplotlib's Circle patch constructor when drawing
        the Particles.

        """

        self.init_particles(n_passive, radius_passive, styles_passive,
                            n_active,  radius_active , styles_active )
                            
        self.ray_freq = ray_freq
        self.rays = []
        for i in range(n_rays):
            # Initialize first rays outside of frame (inactive)
            x = 2.0
            vy = 1.0
            ray = Ray(x, 0, 0, vy, active = False, styles=None)
            self.rays.append(ray)

    def init_particles(self, n_passive, radius_passive, styles_passive,
                            n_active,  radius_active , styles_active ):
        """Initialize the n Particles of the simulation.

        Positions and velocities are chosen randomly; radius can be a single
        value or a sequence with n values.

        """
        def force_iterable_n_times(n, var):
            """Checks if the given variable is iterable of length n.
            
                If so: return the same variable
                If not: return a generator which will return the same
                        value n times.
            """
            try:
                iterator = iter(var)
                assert n == len(var)
            except TypeError:
                # r isn't iterable: turn it into a generator that returns the
                # same value n times.
                def r_gen(n, var):
                    for i in range(n):
                        yield var
                var = r_gen(n, var)
            return var
        
        radius_passive = \
            force_iterable_n_times(n_passive, radius_passive)
        radius_active = \
            force_iterable_n_times(n_active, radius_active)
        
        # Initiate particles
        self.n_passive = n_passive
        self.n_active = n_active
        self.n = n_active + n_passive
        
        self.particles_passive = []
        self.particles_active = []
        self.particles = []
        radius = chain(radius_passive,radius_active) # append 
        
        for i, rad in enumerate(radius):
            # Try to find a random initial position for this particle.
            while True:
                # Choose x, y so that the Particle is entirely inside the
                # domain of the simulation.
                x, y = rad + (1 - 2*rad) * np.random.random(2)
                
                # Choose a random velocity (within some reasonable range of
                # values) for the Particle.
                vr = 0.1 * np.random.random() + 0.05
                vphi = 2*np.pi * np.random.random()
                vx, vy = vr * np.cos(vphi), vr * np.sin(vphi)
                
                # Choose style based on current type of particle
                if i < self.n_passive:
                    styles = styles_passive
                else:
                    styles = styles_active
                
                if i< self.n_passive:
                    particle = Particle(x, y, vx, vy, rad, styles=styles)
                else:
                    particle = Particle(x, y, vx, vy, rad, active=True, styles=styles)
                
                # Check that the Particle doesn't overlap one that's already
                # been placed.
                for p2 in self.particles:
                    if p2.overlaps(particle):
                        break
                else:
                    self.particles.append(particle)
                    if i < self.n_passive:
                        self.particles_passive.append(particle)
                    else:
                        self.particles_active.append(particle)
                    break
        
    def handle_collisions(self):
        """Detect and handle any collisions between the Particles.

        When two Particles collide, they do so elastically: their velocities
        change such that both energy and momentum are conserved.
        
        """
        # Probability of converting between translational & vibrational energy:
        p_e_vt = 0.5
        
        def convert_vibration_translation(p,probability):
            """ Randomly converts all of a particles vibrational energy to
            translational energy or some of translational energy to vibrational
            (Used at collision when resolving momentum & energy transfers)
            """
            
            # The vibrational energy quanta corresponds to the kinetic energy
            # of a particle of radius 0.01 and speed of 0.1.
            vibration_quanta = 0.00001 # = 0.1*0.01^2 
            
            if np.random.random() < probability:
                # Randomly determine if particle wil transfer vibrational
                # energy to translational
                p.vibrating >= 1
                v_abs = np.linalg.norm(p.v)
                v_dir = p.v/v_abs
                m = p.radius**2
                e_k = 0.5*m*v_abs**2
                if p.excited():
                    # Particle vibrates: convert energy to transl. en.
                    e_k_new = e_k + vibration_quanta
                    p.vibrating -= 1
                elif e_k > vibration_quanta:
                    # Particle not vibrating: convert from transl. en.
                    e_k_new = e_k - vibration_quanta
                    p.vibrating = 1
                else:
                    # Particle not vibrating but not enough en. to start.
                    e_k_new = e_k + 0
                
                # Update the translational velocity, based on energy:
                v_abs = np.sqrt(2*e_k_new/m)
                p.v = v_dir*v_abs
        
        def change_velocities(p1, p2):
            """
            Particles p1 and p2 have collided elastically: update their
            velocities.

            """

            m1, m2 = p1.radius**2, p2.radius**2
            M = m1 + m2
            r1, r2 = p1.r, p2.r
            d = np.linalg.norm(r1 - r2)**2
            v1, v2 = p1.v, p2.v
            u1 = v1 - 2*m2 / M * np.dot(v1-v2, r1-r2) / d * (r1 - r2)
            u2 = v2 - 2*m1 / M * np.dot(v2-v1, r2-r1) / d * (r2 - r1)
            p1.v = u1
            p2.v = u2

        # We're going to need a sequence of all of the pairs of particles when
        # we are detecting collisions. combinations generates pairs of indexes
        # into the self.particles list of Particles on the fly.
        pairs = combinations(range(self.n), 2)
        for i,j in pairs:
            if self.particles[i].overlaps(self.particles[j]):
                
                # Check if for vibr./trans. en. conversion:
                if self.particles[i].active:
                    convert_vibration_translation(self.particles[i],p_e_vt)
                if self.particles[j].active:
                    convert_vibration_translation(self.particles[j],p_e_vt)
                    
                # Transfer energy & momentum between particles:
                change_velocities(self.particles[i], self.particles[j])
                
                # Check if for vibr./trans. en. conversion:
                if self.particles[i].active:
                    convert_vibration_translation(self.particles[i],p_e_vt)
                if self.particles[j].active:
                    convert_vibration_translation(self.particles[j],p_e_vt)
                

    def handle_light_absorption(self):
        """Detect and handle light rays hitting particles
        
        When a light ray hits a particle it is absorbed, changing the 
        vibrational energy of the particle (should be visualized as a 
        change in color or momentum(?) )
        """
        for i,ray in enumerate(self.rays):
            for j,particle in enumerate(self.particles_active):
                if ray.hit(particle):
                    # Terminate ray
                    ray.active = False
                    particle.vibrating += 1 # Add vibrational state to particle
        
    def init_ray_ground(self):
        """Finds an unused light ray and re-initializes it at ground level
        Emits a maximum of N light rays at a time.
        """
        N = 1
        for i in range(N):
            if np.random.random() < self.ray_freq:
                for r in self.rays:
                    if r.active == False: 
                        x = np.random.random()
                        y = 0.001
                        vphi = np.random.random()*np.pi/10-np.pi/20+np.pi/2
                        v_abs = 1.0
                        vxy = np.array([np.cos(vphi)*v_abs, np.sin(vphi)*v_abs])
                        
                        # Re-initialize Ray:
                        r.r[0,:] = [x,y]
                        r.r[1,:] = [x,y]
                        r.vxy = vxy
                        r.active = True
                        break
                    else:
                        pass
                
    def init_ray_particle(self):
        """Check for excited (active) particles and randomly emits light
        """
        for i,p in enumerate(self.particles_active):
            if p.excited() and np.random.random() < self.ray_freq:
                # Particle is excited and will emit light
                # (if there are any unused rays)
                for r in self.rays:
                    if r.active == False:
                        p.vibrating -= 1
                        
                        # Pick a direction
                        vphi = np.random.random()*2*np.pi
                        unit_xy = np.array([np.cos(vphi),np.sin(vphi)])
                        
                        # Set ray velocity
                        v_abs = 1.0                    
                        r.vxy = unit_xy * v_abs
                        
                        # Set ray starting point (at edge of particle)
                        r.r[:] = p.radius*unit_xy + p.r    # BC [1,2] -> [2,2]
                        
                        # Set ray as active
                        r.active = True
                        break
                    else:
                        pass

    def init(self):
        """Initialize the Matplotlib animation."""
        
        self.circles = []
        for particle in self.particles:
            self.circles.append(particle.draw(self.ax))
        
        for ray in self.rays:
            self.circles.append(ray.draw(self.ax))
                    
        
        return self.circles
    
    def advance_animation(self, dt):
        """Advance the animation by dt, returning the updated Circles list."""
        for i, p in enumerate(self.particles):
            p.advance(dt)
            self.circles[i].center = p.r
        self.handle_collisions()
        
        self.init_ray_particle()
        self.init_ray_ground()   # initialize a new ground ray (if available)
        for i, r in enumerate(self.rays):
            r.advance(dt)
            xy = self.circles[i+len(self.particles)].get_xy()
            try:
                xy[:] = r.r[:]
            except ValueError:
                exit()
            self.circles[i+len(self.particles)].set_xy(xy)
            """
            if r.active:
                r.advance(dt)
                self.arrows.append(r.draw(self.ax))
            """
        self.handle_light_absorption()
        
        return self.circles
        
    def animate(self, i):
        """The function passed to Matplotlib's FuncAnimation routine."""
        
        self.advance_animation(0.01)
        if DEBUG:
            sleep(0.1)
        return self.circles #(self.circles,self.rays)

    def do_animation(self, save=False):
        """Set up and carry out the animation of the molecular dynamics.

        To save the animation as a MP4 movie, set save=True.
        """

        figsize=(10,10)
        
        fig, self.ax = plt.subplots(figsize=figsize)
        
        fig.subplots_adjust(left=0,bottom=0,right=1,top=1,wspace=None,hspace=None)
        
        for s in ['top','bottom','left','right']:
            self.ax.spines[s].set_linewidth(2)
        self.ax.set_aspect('equal', 'box')

        self.ax.set_xlim(0, 1)
        self.ax.set_ylim(0, 1)
        self.ax.xaxis.set_ticks([])
        self.ax.yaxis.set_ticks([])
        anim = animation.FuncAnimation(fig, self.animate, init_func=self.init,
                               frames=3200, interval=1, blit=True)
        if save:
            Writer = animation.writers['ffmpeg']
            writer = Writer(fps=50, bitrate=3600)
            save_path = '../output/greenhouse-particle-rays.mp4'
            anim.save(save_path, writer=writer)
        else:
            plt.show()


if __name__ == '__main__':
    
    
    # Properties of passive particles
    n_passive = 40
    radii_passive = 0.03
    styles_passive = {'edgecolor': 'C0', 'linewidth': 2, 'fill': None}
    
    # Properties of active particles (greenhouse gases)
    n_active = 5
    radii_active = 0.04
    styles_active = {'edgecolor': '#000000', 'linewidth': 2, 'facecolor': '#800091'}
    
    # Maximum number of simultaneous light rays:
    n_rays = 5
    ray_emit_test = 0.1  # 1.0 = always emitted, 0.0 = never emitted
    
    sim = Simulation(n_passive, radii_passive, styles_passive,
        n_active, radii_active, styles_active, n_rays, ray_emit_test)
    sim.do_animation(save=True)
